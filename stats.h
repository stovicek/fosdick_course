/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material.
 *
 *****************************************************************************/
 /**
  * @file stats.h
  * @brief Coursera assignment file week 1
  *
  * This is an assignment file for a coursera's Introduction to Embedded System
  Software and Development Environments. Nobody will ever see it, because I am
  too cheap to pay for this course.
  *
  * @author Adam Stovicek
  * @date 30.10.2018
  *
  */

#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */

/**
 * @brief Printing array basic statistics including minimum, maximum, mean
  and median
 *
 * <Add Extended Description Here>
 *
 * @param input_array  Input data array to be tested
 * @param <Add InputName> <add description here>
 *
 * @return <Add Return Informaiton here>
 */
 void print_statistics(unsigned char input_array);

 /**
  * @brief Printing the content of the array
  *
  * Printing ALL the content of the array
  *
  * @param input_array
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  *
  * @return 1 is being returned if print proceeded correctly.
  */
 void print_array();

 /**
  * @brief Finding a median value from an array
  *
  * Finding a median value of an input array
  *
  * @param input_array
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  *
  * @return Median value.
  */
 int find_median();

 /**
  * @brief Finding a meab value from an array
  *
  * Finding a mean value of an input array
  *
  * @param input_array
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  *
  * @return Mean value.
  */
 int find_mean();

 /**
  * @brief Finding a maximum value from an array
  *
  * Finding a maximum value of an input array
  *
  * @param input_array
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  *
  * @return Maximum value.
  */
 int find_maximum();

 /**
  * @brief Finding a minimum value from an array
  *
  * Finding a minimum value of an input array
  *
  * @param input_array
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  *
  * @return Minimum value.
  */
 int find_minimum();

 /**
  * @brief Sorting array
  *
  * Sorting an input array
  *
  * @param input_array
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  * @param <Add InputName> <add description here>
  *
  * @return Sorted array.
  */
 int sort_array();

#endif /* __STATS_H__ */
